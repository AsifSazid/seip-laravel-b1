<?php
 
namespace App\View\Composers;

use App\Models\Category;
use Illuminate\Support\Facades\Cookie;
use Illuminate\View\View;
 
class FrontendComposer
{

    protected $categories;
 
    public function __construct()
    {
        // Dependencies are automatically resolved by the service container...
        $this->categories = $this->getCategories();
    }

    public function setCategories()
    {
       return $this->categories = Category::all();
    }

    public function getCategories()
    {
        return $this->setCategories();
    }
 
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $productIds = Cookie::get('productIds');
        $countProductsInCart = is_array(unserialize($productIds)) ? count(unserialize($productIds)) : 0;
        
        $view->with([
            'categories' => $this->categories,
            'countProductsInCart' => $countProductsInCart
        ]);
    }
}