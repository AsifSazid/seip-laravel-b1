<?php

function sendSMS($mobile_no = null, $message = null)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://sms1.pondit.com/smsapi?api_key=your_api_key_here&type=text&contacts=" . $mobile_no . "&senderid=your_sender_id_here&msg=" . urlencode($message),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        //echo "cURL Error #:" . $err;
        return false;
    } else {
        //echo $response;
        return true;
    }
}


